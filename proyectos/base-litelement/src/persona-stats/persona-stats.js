import {LitElement,html} from 'lit-element';



class PersonaStats extends LitElement{

    static get properties(){
        return{
           people:{type:Array}
        };
    }

     constructor(){
       super();  
       this.people =[];
     }

     update(changedProperties){
         console.log("update en persona-stats");
         console.log(changedProperties);
         if (changedProperties.has("people")){
             console.log("ha cambiado el valor de la propiedad people en persona-stats");
             let peopleStats = this.gatherPeopleArraysInfo(this.people);
             this.dispatchEvent(new customElements("updated-people-stats",
             {
                 detail:{
                     peopleStats: peopleStats
                 }
             }
             ))

         }
     }
     gatherPeopleArraysInfo(people){
         console.log("gatherPeopleArraysInfo en persona-stats");
         let peopleStats = {};
         people           = this.gatherPeopleArraysInfo(this.people)
     }
}

customElements.define('persona-stats',PersonaStats)